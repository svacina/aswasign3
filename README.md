# Install

`mvn clean install`


# Run

The project is not meant to run, just make use of `JaxbService`. See below.

# Test

`mvn clean test`. There is a test that generates sample output in the root of directory.

