package edu.baylor.ecs.aswasign3;

import edu.baylor.ecs.aswasign3.metamodel.MetaContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Aswasign3Service {

    @Autowired
    private FileService fileService;

    @Autowired
    private JaxbService jaxbService;

    public void createModel(List<Object> entities){
        MetaContext metaContext = jaxbService.getContext(entities);
        fileService.writeToFile(metaContext.toString(), metaContext.getStringDate());
    }
}
