package edu.baylor.ecs.aswasign3;

import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class FileService {

    public void writeToFile(String content, String fileName){
        try{

            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(content);
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}
