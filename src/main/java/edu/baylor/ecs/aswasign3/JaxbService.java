package edu.baylor.ecs.aswasign3;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.baylor.ecs.aswasign3.metamodel.MetaCollection;
import edu.baylor.ecs.aswasign3.metamodel.MetaContext;
import edu.baylor.ecs.aswasign3.metamodel.fields.MetaCustomField;
import edu.baylor.ecs.aswasign3.metamodel.MetaEntity;
import edu.baylor.ecs.aswasign3.metamodel.fields.MetaDateField;
import edu.baylor.ecs.aswasign3.metamodel.fields.StringFieldMeta;
import edu.baylor.ecs.aswasign3.metamodel.iMetaRecord;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

//ToDo: decide type of field (enum, string / number, collection, object)
//ToDo: recursive method with stack of types

@Service
public class JaxbService {

    public String startParsing(){
        return "start";
    }


    public MetaContext getContext(List<Object> entities){
        MetaContext metaContext = new MetaContext();
        // set entities
        metaContext.setRecordList(getMetaEntities(entities));
        // for each entity
        // decide if collection, field, ...

        //List<iMetaRecord> recordList = getRecordList(entities);
        //metaContext.setRecordList();
        return metaContext;
    }

    /**
     * Set all entities
     * @param entities
     * @return
     */
    public MetaCollection getMetaCollection(List<Object> entities, String name){
        MetaCollection metaEntities = new MetaCollection();
        metaEntities.setTagName(name);
        for (Object entity: entities
        ) {
            metaEntities.setEntity(getMetaEntity(entity));
        }
        return metaEntities;
    }


    /**
     * Set all entities
     * @param entities
     * @return
     */
    public List<iMetaRecord> getMetaEntities(List<Object> entities){
        List<iMetaRecord> metaEntities = new ArrayList<>();
        for (Object entity: entities
             ) {
            metaEntities.add(getMetaEntity(entity));
        }
        return metaEntities;
    }

    /**
     * Set one entity
     * @param entity
     * @return
     */
    public iMetaRecord getMetaEntity(Object entity){
        iMetaRecord metaEntity = new MetaEntity();
        ((MetaEntity) metaEntity).setTagName(getEntityName(entity));
        ((MetaEntity) metaEntity).setId(getEntityId(entity));
        ((MetaEntity) metaEntity).setFields(getMetaFields(entity));
        return metaEntity;
    }

    /**
     * Get all fields for entity
     * @param entity
     * @return
     */
    private List<iMetaRecord> getMetaFields(Object entity) {
        List<iMetaRecord> metaFields = new ArrayList<>();
        Class entityClass = getEntityClass(entity);
        Field[] allFields = entityClass.getDeclaredFields();
        for (Field field: allFields
        ) {
            if (!field.getName().equals("id")) {
                try {
                    field.setAccessible(true);
                    Object objValue = field.get(entity);
                    if (objValue == null){
                        //return null;
                    } else {
                        iMetaRecord metaRecord = getMetaField(field, entity);
                        if (metaRecord != null) {
                            metaFields.add(metaRecord);
                        }

                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
        return metaFields;
    }

    /**
     * Get one field
     * @param field
     * @param entity
     * @return
     */
    private iMetaRecord getMetaField(Field field, Object entity) {

        iMetaRecord iMetaRecord = null;

        field.setAccessible(true);
        //get field name
        String methodName = getMethodName(field);
        //get all methods
        Method[] methods = getClassMethods(entity, field);
        // find corresponding method
        Method method = getMethodByName(methods, methodName);
        // set annotation on getter method
        MetaCustomField metaCustomField = getMethodAttributes(method);

        // here function that creates respective objects

        iMetaRecord = createCustomType(metaCustomField, field, entity);

        return iMetaRecord;
    }


    public MetaCustomField getMethodAttributes(Method method){
        MetaCustomField metaCustomField = new MetaCustomField();
        Column column = method.getAnnotation(Column.class);

        if (column != null){
            if(column.name().equals("")){
                metaCustomField.setTagName(column.name());
            }
            if (column.nullable()){
                metaCustomField.setNotNull(column.nullable());
                metaCustomField.appendAttributeString("notNull", "true");
            }
            if (column.length() > 0){
                metaCustomField.setLength(column.length());
                metaCustomField.appendAttributeString("length", column.length());
            }
            if (column.unique()){
                metaCustomField.setUnique(column.unique());
            }
            if (column.updatable()){
                metaCustomField.setUpdatable(column.updatable());
            }
        }

        Min min = method.getAnnotation(Min.class);
        if (min != null) {
            metaCustomField.setMin(min.value());
            metaCustomField.appendAttributeString("min", min.value());
        }

        Max max = method.getAnnotation(Max.class);
        if (max != null){
            metaCustomField.setMax(max.value());
            metaCustomField.appendAttributeString("max", max.value());
        }

        NotEmpty notEmpty = method.getAnnotation(NotEmpty.class);
        if (notEmpty != null){
            metaCustomField.setNotEmpty(true);
            metaCustomField.appendAttributeString("notEmpty", "true");
        }

        JsonFormat jsonFormat = method.getAnnotation(JsonFormat.class);
        if (jsonFormat != null) {
            metaCustomField.setFormat(jsonFormat.pattern());
            metaCustomField.appendAttributeString("format", jsonFormat.pattern());
        }

        ManyToOne manyToOne = method.getAnnotation(ManyToOne.class);
        if (manyToOne != null) {
            metaCustomField.setObject(true);
        }

        return metaCustomField;
    }


    private iMetaRecord createCustomType(MetaCustomField metaCustomField, Field field, Object entity) {
        Class<?> type2 = field.getType();
        // Looking for array
        if(type2.equals(java.util.List.class)){
            List<Object> objValue;
            try {
                objValue = (List<Object>) field.get(entity);
                return getMetaCollection(objValue, field.getName());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        // recursion on entity!
        if (metaCustomField.isObject()){
            Object objValue;
            try {
                objValue = field.get(entity);
                iMetaRecord mm = getMetaEntity(objValue);
                return mm;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        StringFieldMeta stringFieldMeta = new StringFieldMeta(metaCustomField);
        stringFieldMeta.setTagName(field.getName());
        try {
            stringFieldMeta.setValue(field.get(entity).toString());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return stringFieldMeta;

    }

    private Long getEntityId(Object entity) {
        Object val = null;

        for (Field field: getEntityClass(entity).getDeclaredFields()
        ) {
            if (field.getName().equals("id")) {
                try {
                    field.setAccessible(true);
                    val = field.get(entity);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return (Long) val;
    }

    public String getEntityName(Object entity){
        String allName = entity.getClass().getName();
        return allName.substring(allName.lastIndexOf('.' ) + 1 );
    }

    public Class getEntityClass(Object entity){
        return entity.getClass();
    }

    public Method[] getClassMethods(Object entity, Field field){
        Class entityClass = entity.getClass();
        return entityClass.getDeclaredMethods();
    }

    public Method getMethodByName(Method[] methods, String methodName){
        return Arrays.stream(methods).filter(n -> n.getName().equals(methodName)).findFirst().get();
    }

    public String getMethodName(Field field){
        String capitalName = capitalName(field.getName());
        Class tClass = field.getType();
        if (tClass.equals(boolean.class) || tClass.equals(Boolean.class)){
            return "is" + capitalName;
        }
        return "get" + capitalName;
    }

    public String capitalName(String fieldName) {
        return fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }


}
