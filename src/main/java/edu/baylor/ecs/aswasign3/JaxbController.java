package edu.baylor.ecs.aswasign3;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JaxbController {

    @RequestMapping("/")
    public String home(){
        return "Hello";
    }
}
