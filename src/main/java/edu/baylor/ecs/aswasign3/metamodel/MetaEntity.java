package edu.baylor.ecs.aswasign3.metamodel;

import java.util.ArrayList;
import java.util.List;

public class MetaEntity implements iMetaRecord {
    private String tagName;
    private Long id;
    private List<iMetaRecord> fields;

    public MetaEntity() {
        this.fields = new ArrayList<>();
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<iMetaRecord> getFields() {
        return fields;
    }

    public void setFields(List<iMetaRecord> fields) {
        this.fields = fields;
    }

    public void setField(iMetaRecord field){
        this.fields.add(field);
    }

    @Override
    public String toString() {
//        if (fields == null){
//            return "";
//        } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<");
            stringBuilder.append(tagName);
            stringBuilder.append(" id=\"");
            stringBuilder.append(id);
            stringBuilder.append("\">\n");
            if (fields != null){
                for (iMetaRecord record: fields
                ) {
                    stringBuilder.append(record.toString());
                }
            }

            stringBuilder.append("</");
            stringBuilder.append(tagName);
            stringBuilder.append(">\n");
            return stringBuilder.toString();
//        }
    }
}
