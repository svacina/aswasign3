package edu.baylor.ecs.aswasign3.metamodel.fields;


import edu.baylor.ecs.aswasign3.metamodel.iMetaRecord;

public class MetaCustomField implements iMetaRecord {
    private Long max;
    private Long min;
    private Long length;
    private boolean notNull;
    private boolean notEmpty;
    private String tagName;
    private boolean unique;
    private boolean updatable;
    private String attributeString;
    private String format;
    private boolean isObject = false;

    public MetaCustomField() {
        this.attributeString = "";
    }

    public MetaCustomField(String attributeString) {
        this.attributeString = attributeString;
    }

    public MetaCustomField(Long max, Long min, Long length, boolean notNull, boolean notEmpty, String tagName, boolean unique, boolean updatable, String attributeString) {
        this.max = max;
        this.min = min;
        this.length = length;
        this.notNull = notNull;
        this.notEmpty = notEmpty;
        this.tagName = tagName;
        this.unique = unique;
        this.updatable = updatable;
        this.attributeString = attributeString;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public boolean isUpdatable() {
        return updatable;
    }

    public void setUpdatable(boolean updatable) {
        this.updatable = updatable;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public boolean isNotEmpty() {
        return notEmpty;
    }

    public void setNotEmpty(boolean notEmpty) {
        this.notEmpty = notEmpty;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getAttributeString() {
        return attributeString;
    }

    public void setAttributeString(String attributeString) {
        this.attributeString = attributeString;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isObject() {
        return isObject;
    }

    public void setObject(boolean object) {
        isObject = object;
    }

    public String getTheFirstString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<");
        stringBuilder.append(tagName);
        stringBuilder.append(attributeString);
        stringBuilder.append(">");
        return stringBuilder.toString();
    }

    public String getTheLastString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("</");
        stringBuilder.append(tagName);
        stringBuilder.append(">");
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    public void appendAttributeString(String key, String value){
        this.attributeString += attributeToString(key, value);
    }

    public void appendAttributeString(String key, Integer value){
        this.attributeString += attributeToString(key, value.toString());
    }

    public void appendAttributeString(String key, long value){
        this.attributeString += attributeToString(key, value);
    }

    private String attributeToString(String key, String value){
        return " " + key + "=" + "\"" + value + "\"";
    }

    private String attributeToString(String key, long value){
        return " " + key + "=" + "\"" + value + "\"";
    }
}
