package edu.baylor.ecs.aswasign3.metamodel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MetaContext {

    private String pattern;
    private SimpleDateFormat simpleDateFormat;
    private String stringDate;
    private Date date;
    private List<iMetaRecord> recordList;

    public MetaContext(){
        this.date = new Date();
        this.pattern = "yyyy-MM-dd";
        this.simpleDateFormat = new SimpleDateFormat(pattern);
        this.stringDate = simpleDateFormat.format(this.date);
    }

    public MetaContext(List<iMetaRecord> recordList){
        this.date = new Date();
        this.pattern = "yyyy-MM-dd";
        this.simpleDateFormat = new SimpleDateFormat(pattern);
        this.stringDate = simpleDateFormat.format(this.date);
        this.recordList = recordList;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public SimpleDateFormat getSimpleDateFormat() {
        return simpleDateFormat;
    }

    public void setSimpleDateFormat(SimpleDateFormat simpleDateFormat) {
        this.simpleDateFormat = simpleDateFormat;
    }

    public String getStringDate() {
        return stringDate;
    }

    public void setStringDate(String stringDate) {
        this.stringDate = stringDate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<iMetaRecord> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<iMetaRecord> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<xml reportedOn=");
        stringBuilder.append(stringDate);
        stringBuilder.append("\"\n");
        for (iMetaRecord record: recordList
             ) {
            stringBuilder.append(record.toString());
        }
        return stringBuilder.toString();
    }




}
