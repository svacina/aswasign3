package edu.baylor.ecs.aswasign3.metamodel.fields;

import java.util.Date;

public class MetaDateField extends MetaCustomField {

    private Date value;

    public MetaDateField(MetaCustomField metaCustomField) {
        super();
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getTheFirstString());
        stringBuilder.append(value.toString());
        stringBuilder.append(this.getTheLastString());
        return stringBuilder.toString();
    }
}
