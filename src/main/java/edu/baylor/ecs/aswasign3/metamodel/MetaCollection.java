package edu.baylor.ecs.aswasign3.metamodel;

import java.util.ArrayList;
import java.util.List;

public class MetaCollection implements iMetaRecord {

    private String tagName;

    private List<iMetaRecord> entities = new ArrayList<>();

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public List<iMetaRecord> getEntities() {
        return entities;
    }

    public void setEntities(List<iMetaRecord> entities) {
        this.entities = entities;
    }

    public void setEntity(iMetaRecord metaEntity){
        this.entities.add(metaEntity);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<");
        stringBuilder.append(tagName);
        stringBuilder.append(">");
        stringBuilder.append("\n");
        for (iMetaRecord metaEntity : entities
             ) {
            stringBuilder.append(metaEntity.toString());
        }
        stringBuilder.append("</");
        stringBuilder.append(tagName);
        stringBuilder.append(">");
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
