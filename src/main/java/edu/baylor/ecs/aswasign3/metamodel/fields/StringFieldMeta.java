package edu.baylor.ecs.aswasign3.metamodel.fields;

public class StringFieldMeta extends MetaCustomField {

    private String value;

    public StringFieldMeta(){

    }

    public StringFieldMeta(MetaCustomField metaCustomField) {
        super(metaCustomField.getAttributeString());

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getTheFirstString());
        stringBuilder.append(value);
        stringBuilder.append(this.getTheLastString());
        return stringBuilder.toString();
    }
}
