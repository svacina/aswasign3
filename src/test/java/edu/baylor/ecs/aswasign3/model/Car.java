package edu.baylor.ecs.aswasign3.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Car {
    private Long id;
    private String brand;
    private String type;

    @Id
    @SequenceGenerator(
            name = "category_sequence",
            allocationSize = 1,
            initialValue = 1020
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_sequence")
    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name = "brand", nullable = false, length = 100)
    @NotEmpty
    @Size(max = 100)
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Column(name = "type", nullable = false, length = 100)
    @NotEmpty
    @Size(max = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
