package edu.baylor.ecs.aswasign3.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.*;


import java.util.Date;
import java.util.List;

@Entity
public class Person implements iEntity {
    private Person supervisor;
    private List<Car> cars;
    private Long id;
    private String name;
    private Date born;
    private Gender gender;
    private Integer salary;



    public Person() {
    }

    public Person(Long id, String name, Date born, Gender gender, Integer salary) {
        this.id = id;
        this.name = name;
        this.born = born;
        this.gender = gender;
        this.salary = salary;
    }

    public Person(Long id, String name, Date born, Gender gender, Integer salary, List<Car> cars) {
        this.id = id;
        this.name = name;
        this.born = born;
        this.gender = gender;
        this.salary = salary;
        this.cars = cars;
    }

    public Person(List<Car> cars, Person supervisor, Long id, String name, Date born, Gender gender, Integer salary) {
        this.cars = cars;
        this.supervisor = supervisor;
        this.id = id;
        this.name = name;
        this.born = born;
        this.gender = gender;
        this.salary = salary;
    }

    @Override
    @Id
    @SequenceGenerator(
            name = "category_sequence",
            allocationSize = 1,
            initialValue = 1020
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_sequence")
    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id){
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 100)
    @NotEmpty
    @Size(max = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "born", nullable = false)
    @Temporal(TemporalType.DATE)
    @NotEmpty
    @Past
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ", locale = "en_GB")
    public Date getBorn() {
        return this.born;
    }

    public void setBorn(Date born) {
        this.born = born;
    }

    @Column(name = "salary", nullable = false)
    @Min(1000)
    @Max(15000)
    @NotEmpty
    public Integer getSalary() {
        return this.salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Column(name = "gender", nullable = false)
    @Enumerated(EnumType.STRING)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    public List<Car> getCars() {
        return cars;
    }


    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    public Person getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Person supervisor) {
        this.supervisor = supervisor;
    }
}
