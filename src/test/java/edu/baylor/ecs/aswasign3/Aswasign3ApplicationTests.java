package edu.baylor.ecs.aswasign3;

import edu.baylor.ecs.aswasign3.metamodel.MetaContext;
import edu.baylor.ecs.aswasign3.model.Car;
import edu.baylor.ecs.aswasign3.model.Gender;
import edu.baylor.ecs.aswasign3.model.Person;
import edu.baylor.ecs.aswasign3.model.iEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
public class Aswasign3ApplicationTests {

	@Autowired
	JaxbService helloService;

	@Autowired
	FileService fileService;

	@Autowired
	Aswasign3Service aswasign3Service;


	@DisplayName("Generating model")
	@Test
	void generateModel(){

		Car car = new Car();
		car.setId(new Long(1));
		car.setBrand("Skoda");
		car.setType("120L");
		Car car2 = new Car();
		car2.setId(new Long(2));
		car2.setBrand("Trabi");
		car2.setType("M3");
		List<Car> cars = new ArrayList<>();
		cars.add(car);
		cars.add(car2);
		Person stransky = new Person(new Long(3),"Stranskej", new Date(), Gender.MALE, 1000, null);
		Person person = new Person(cars, stransky, new Long(1),"Jiri Kara", new Date(), Gender.MALE, 1000);
		Person person1 = new Person(cars, stransky, new Long(2),"Blanka Divisova", new Date(), Gender.FEMALE, 1001);
		List<Object> entities = new ArrayList<>();
		entities.add(person);
//		entities.add(person1);
		this.aswasign3Service.createModel(entities);
	}





}
